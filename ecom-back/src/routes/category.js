const express = require('express');
const { requireSignin, adminMiddleware } = require('../common-middleware');
const { addCategory, getCategories, updateCategories, deleteCategories } = require('../controller/category');
const router = express.Router();
const shortid = require ('shortid');
const path = require ('path');
const multer = require ('multer');
// const slugify = require('slugify');
// const Category = require ('../models/category');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(path.dirname(__dirname), 'uploads'))
    },
    filename: function (req, file, cb) {
      cb(null, shortid.generate() + '-' + file.originalname)
    }
  });

  const upload = multer ({storage}); 


router.post('/category/create', requireSignin, adminMiddleware, upload.single('categoryImage'), addCategory);
router.get('/category/getCategory', getCategories);
router.post('/category/update', upload.array('categoryImage'), updateCategories);
router.post('/category/delete', deleteCategories);

// router.post('/category/create', (req, res)=>{

//   const categoryObj ={

//     name: req.body.name,
//     slug: slugify(req.body.name)
//   }

//   if (req.body.parentId){
//     categoryObj.parentId = req.body.parentId;
//   }

//   const cat = new Category(categoryObj);
//   cat.save((error, category)=>{
//     if(error) return res.status(400).json({error});
//     if(category){
//       return res.status(201).json({category});
//     }
//   });

// });



module.exports = router;